package cheezbags;

//import javax.annotation.Nullable;

import org.bukkit.Material;
//import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

//import net.minecraft.server.v1_16_R3.NBTTagCompound;
//import net.minecraft.server.v1_16_R3.NBTTagString;

public class Utilities {
    
    public static boolean isEmpty(ItemStack item) {
        return item == null || isAir(item);
    }
    
    public static boolean isAir(ItemStack item) {
        return isAir(item.getType());
    }
    
    public static boolean isAir(Material m) {
        return m == Material.AIR || m == Material.CAVE_AIR;
    }
    
//    /**
//     * @return A copy of the NMS ItemStack.
//     */
//    public static net.minecraft.server.v1_16_R3.ItemStack asNMS(ItemStack item) {
//        if (Utilities.isEmpty(item))
//            return null;
//        
//        return CraftItemStack.asNMSCopy(item);
//    }
//
//    /**
//     * @return Gets a copy of the NBTTagCompound held by the item.
//     */
//    public static @Nullable NBTTagCompound getTag(ItemStack item) {
//        if (Utilities.isEmpty(item))
//            return null;
//        
//        net.minecraft.server.v1_16_R3.ItemStack stack = asNMS(item);
//        return stack.hasTag() ? stack.getTag() : null;
//    }
//    
//    public static String getNBTValueStringOrDefault(ItemStack item, String key, String def) {
//        NBTTagCompound tag = getTag(item);
//        if (tag == null)
//            return def;
//        
//        if (!tag.hasKey(key))
//            return def;
//        
//        return tag.getString(key);
//    }
//
//    /**
//     * @return A Bukkit ItemStack after the specified changes. The original ItemStack is not changed.
//     */
//    public static ItemStack setTag(ItemStack item, NBTTagCompound tag) {
//        if (Utilities.isEmpty(item))
//            return null;
//        
//        if (tag == null)
//            tag = new NBTTagCompound();
//
//        net.minecraft.server.v1_16_R3.ItemStack stack = asNMS(item);
//        stack.setTag(tag);
//        return CraftItemStack.asCraftMirror(stack);
//    }
//
//    /**
//     * @return A Bukkit ItemStack after the specified changes. The original ItemStack is not changed.
//     */
//    public static ItemStack setNBTValue(ItemStack item, String key, String value) {
//        NBTTagCompound tag = getTag(item);
//        if (tag == null)
//            tag = new NBTTagCompound();
//        
//        tag.set(key, NBTTagString.a(value));
//        item = setTag(item, tag);
//        return item;
//    }
    
}
