package cheezbags.events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class MysteryBagRNGEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Entity killed;
    private boolean cancelled;
    private double chanceBoost;
 
    public MysteryBagRNGEvent(Player player, Entity killed) {
        this.player = player;
        this.killed = killed;
    }
    
    public void addChanceBoost(double amt) {
        chanceBoost += amt;
    }
    
    public double getChanceBoostRaw() {
        return chanceBoost;
    }
 
    public Player getPlayer() {
        return player;
    }
    
    public Entity getKilledEntity() {
        return killed;
    }
    
    @Override
    public boolean isCancelled() {
        return cancelled;
    }
 
    @Override
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
 
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
